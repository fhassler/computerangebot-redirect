Einführung in Betriebssysteme und Linux

----
#### Was ist ein Betriebssystem?
- Ein Betriebssystem (OS = operating system) ist eine Software, die die Hardware eines Computers verwaltet.
- Es ermöglicht die Ausführung von Anwendungen und bietet eine Benutzeroberfläche.

----
#### Funktionen eines Betriebssystems
- **Hardwareverwaltung:** Steuert und verwaltet Hardwarekomponenten (z.B. CPU, RAM, Festplatten).
- **Benutzeroberfläche:** Bietet eine Schnittstelle für Benutzende (z.B. grafisch oder textbasiert).
- **Dateiverwaltung:** Organisiert und verwaltet Dateien und Verzeichnisse.
- **Multitasking:** Ermöglicht die gleichzeitige Ausführung mehrerer Programme.

----
#### Beispiele für Betriebssysteme
- **Windows:** Weit verbreitet, besonders in Büros und bei Spielen.
- **macOS:** Betriebssystem von Apple für Mac-Computer.
- **Linux:** Open-Source-Betriebssystem, das in vielen Varianten existiert.
- **Android:** Betriebssystem für mobile Geräte.
- **iOS:** Betriebssystem von Apple für iPhones und iPads.

----
####  Fokus auf Linux
- **Was ist Linux?**
  - Ein Open-Source-Betriebssystem, das von einer Community entwickelt wird.
- **Vorteile von Linux:**
  - Kostenfrei und anpassbar.
  - Hohe Sicherheit(?) und Stabilität.
  - Große Auswahl an Distributionen (z.B. Ubuntu, Fedora).

----
#### Warum Linux verwenden?
- **Open Source:** Jeder kann den Quellcode einsehen und ändern.
- **Sicherheit:** Dadurch theoretisch weniger anfällig für Viren und Malware.
- **Anpassungsfähigkeit:** Ideal für Server, Desktops und IoT-Geräte.
- **Community:** Unterstützung durch eine große und aktive Community.

----
- **Was ist Ubuntu?**
  - Eine der bekanntesten Linux-Distributionen.
  - Ideal für Anfänger und erfahrene Benutzer.

- **Hauptmerkmale:**
  - Benutzerfreundliche Oberfläche (GNOME).
  - Große Community und viele Ressourcen.

----
- **Umfangreiche Softwarebibliothek:**
  - Zugriff auf Tausende von Anwendungen über das Software-Center.

- **Starke Community:**
  - Viele Tutorials, Foren und Unterstützung verfügbar.

----
- **Was ist Solus?**
  - Eine benutzerfreundliche Linux-Distribution.
  - Entwickelt für Desktop-Nutzer.

- **Hauptmerkmale:**
  - Rolling Release: Immer die neuesten Updates.
  - Standard-Desktop: Budgie, einfach und modern.

----
- **Einfache Installation:**
  - Benutzerfreundlicher Installationsprozess.

- **Softwareverwaltung:**
  - Eopkg-Paketmanager für einfache Softwareinstallation.
  - Unterstützung für Flatpak-Anwendungen.


----
- **Solus oder Ubuntu?**
  - Was sind die Vorlieben der Gruppe?
  - Welche Funktionen sind für uns wichtig?
  - Diskutieren wir die Vor- und Nachteile!
