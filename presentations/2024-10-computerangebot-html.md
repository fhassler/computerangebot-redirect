# Einführung HTML
## Was arbeitet Florian?

----
### Was ist ein Texteditor?
- Ein Programm, mit dem wir Texte schreiben können.
- Beispiele: Notepad (Windows), Notepad++, Visual Studio Code.
- Wir verwenden den Texteditor, um HTML-Code zu schreiben.

----
### Was ist ein Browser?
- Ein Browser ist ein Programm, mit dem wir Webseiten ansehen können.
- Beispiele: Google Chrome, Mozilla Firefox, Microsoft Edge, Safari.
- Der Browser zeigt den HTML-Code an und macht ihn für uns sichtbar.
- Mit einem Browser können wir im Internet surfen, Bilder ansehen und Videos schauen.

----
### Was ist HTML?
- HTML steht für "Hypertext Markup Language".
- Es ist die Sprache, die wir verwenden, um Webseiten zu erstellen.
- HTML sagt dem Browser, wie die Inhalte angezeigt werden sollen.

----
### HTML-Struktur

```html
<!DOCTYPE html>
<html>
<head>
    <title>Meine erste Webseite</title>
</head>
<body>
    <h1>Willkommen auf meiner Webseite!</h1>
    <p>Das ist ein einfacher Absatz.</p>
</body>
</html>
```

----
### HTML-Struktur
- `<!DOCTYPE html>`: Definiert den Dokumenttyp.
- `<html>`: Wurzel-Element des HTML-Dokuments.
	- `<head>`: Enthält Metainformationen.
		- `<title>`: Titel der Webseite.
	- `<body>`: Hauptinhalt der Webseite.
		- `<h1>`, `<p>`: Überschrift und Absatz.

----
### HTML-Elemente

- `<h1>` bis `<h6>`: Überschriften
- `<p>`: Absatz
- `<strong>`: Fettgedruckt
- `<em>`: Kursiv
- **Ungeordnete Liste**: `<ul>`, `<li>`
- **Geordnete Liste**: `<ol>`, `<li>`

----
### Links und Bilder

- **Link**: `<a href="URL">Linktext</a>`
- **Bild**: `<img src="Bild-URL" alt="Beschreibung">`

----
### Aufgabe
1. Öffne Notepad auf deinem Windows-Computer.
2. Schreibe den Code von vorhin rein (nächste Folie).
3. Speichere die Datei als `meine_webseite.html`.
4. Öffne die Datei in deinem Webbrowser.

----
```html
<!DOCTYPE html>
<html>
<head>
    <title>Meine erste Webseite</title>
</head>
<body>
    <h1>Willkommen auf meiner Webseite!</h1>
    <p>Das ist ein einfacher Absatz.</p>
</body>
</html>
```

----
### Bonus
```html
<a href="https.//freie-schule.at">Besuch unsere Webseite</a>


<img src="https://picsum.photos/200" alt="zufälliges Foto">

<ul>
  <li>Listenpunkt 1</li>
  <li>noch ein Punkt</li>
</ul>									
```

----
### Zusammenfassung
- HTML ist die Sprache für Webseiten.
- Wir verwenden einen Texteditor, um HTML zu schreiben.
- Mit HTML können wir Texte, Überschriften und Absätze erstellen.
- Jetzt bist du bereit, deine eigene Webseite zu erstellen!