const fs = require('fs');
const path = require('path');

const presentationsDir = 'presentations';
const indexFilePath = 'index.html';

const files = fs.readdirSync(presentationsDir).filter(file => file.endsWith('.md'));

let listItems = files.map(file => {
    const markdown = fs.readFileSync(path.join(presentationsDir, file), 'utf8');
    const firstLine = markdown.split('\n')[0]; 
    const title = firstLine.replace(/^#\s*/, '');
    const link = file.replace('.md', '.html');
    return `<li><a class="button" href="${link}">${title}</a></li>`;
}).join('\n');

const indexContent = `
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Computerangebot - Freie Schule Linz</title>
    <meta name="description" content="Folien zum Computerangebot in der Freien Schule Linz">
    <meta name="keywords" content="Computerangebot, Präsentationen">
    <meta name="robots" content="index, follow">
    <link rel="stylesheet" href="css/landing.css">
</head>

<body>
    <h1>Computerangebot 2024</h1>
    <p>Hier findet ihr eine Sammlung von Präsentationen zum Computerangebot.</p>
    <p>Das Copyright für alle Folien liegt bei Florian.</p>
    <ul>
        ${listItems}
    </ul>
    <p>Viel Spaß beim Stöbern!</p>
</body>
</html>
`;

fs.writeFileSync(indexFilePath, indexContent);
