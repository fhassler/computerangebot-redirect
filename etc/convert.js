const fs = require('fs');
const md = require('markdown-it')();

const presentationsDir = 'presentations';
const outputDir = '.';

fs.readdirSync(presentationsDir).forEach(file => {
    if (file.endsWith('.md')) {
        const markdown = fs.readFileSync(`${presentationsDir}/${file}`, 'utf8');
        const sections = markdown.split(/----/); // Teile den Markdown-Text in Abschnitte

        // Wandle jeden Abschnitt in HTML um und umschließe ihn mit <section>
        const htmlSections = sections.map(section => {
            const trimmedSection = section.trim();
            if (trimmedSection) {
                return `<section>${md.render(trimmedSection)}</section>`;
            }
            return '';
        }).join('\n'); // Füge die Abschnitte zusammen

        const filename = file.replace('.md', '.html');

        const output = `
        <!doctype html>
        <html>
        <head>
            <link rel='stylesheet' href='css/reveal.css'>
            <link rel='stylesheet' href='css/reset.css'>
            <link rel='stylesheet' href='css/simple.css'>
            <script src='js/reveal.js'></script>
        </head>
        <body>
            <div class='reveal'>
                <div class='slides'>
                    ${htmlSections} <!-- Füge die Folien hier ein -->
                </div>
            </div>
            <script>
                Reveal.initialize();
            </script>
        </body>
        </html>
        `;

        fs.writeFileSync(`${outputDir}/${filename}`, output);
    }
});
